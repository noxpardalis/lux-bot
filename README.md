# Lux Bot

## Run the bot (native)

### Build the bot
The bot is built using `cargo`. Installing the Rust toolchain using
[rustup](https://rustup.rs) provides access to `cargo`. With `rustup` installed simply execute the following commands to build the bot.

```sh
rustup install stable # Install the stable toolchain
rustup default stable # Set the toolchain to use stable
cargo build --release
```

### Start the bot
To run the bot set the `DISCORD_TOKEN` environment variable with your bot token. You could also add it to a `.env` file at the root of the repository. A example is included at `.env.example`. Then execute the bot:
```sh
cargo run --release
```

Alternatively, you could manually execute the binary present in the `target` directory (e.g. `./target/release/lux_bot`).

## Run the bot (docker)
A `Dockerfile` is included in the repository.

### Build the bot
```sh
docker build -t lux-bot .
```

### Start the bot
```sh
docker run -d ---name lux-bot -env-file .env lux-bot
```
